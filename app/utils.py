import html
from typing import Any


def escape(txt: Any) -> str:
    return html.escape(str(txt))


def compare_answer(answer: str, correct_answer: str) -> bool:
    # Если участник вводит текстом слово токамак, Токамак или ТОКАМАК,
    # то ответ считается правильным
    return (
        answer == correct_answer.lower()
        or answer.capitalize() == correct_answer.capitalize()
        or answer.upper() == correct_answer.upper()
    )
